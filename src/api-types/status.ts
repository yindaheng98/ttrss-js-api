/**
 * Status for subscribeToFeed and other API.
 */
export class Status {
    code: number = 0;
    message: string = '';
}
